# Baixando pacote

echo -e "\n\n Iniciando instalação do Maven 3.6.3 \n\n"

wget --progress=bar http://ftp.unicamp.br/pub/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz -O ~/Desenv/java/apache-maven-3.6.3-bin.tar.gz

tar -zxvf ~/Desenv/java/apache-maven-3.6.3-bin.tar.gz -C ~/Desenv/java/

rm ~/Desenv/java/apache-maven-3.6.3-bin.tar.gz

echo -e "\n #### Variáveis setadas pelo script de instalação do Maven \n" >> ~/.profile

echo 'MAVEN_HOME=~/Desenv/java/apache-maven-3.6.3' >> ~/.profile
echo 'M3_HOME=$MAVEN_HOME' >> ~/.profile
echo 'PATH=$PATH:$MAVEN_HOME/bin' >> ~/.profile

echo -e "\n ########################################################### \n" >> ~/.profile

source ~/.profile

mvn --version

echo -e "\n Instalação do Maven completa."