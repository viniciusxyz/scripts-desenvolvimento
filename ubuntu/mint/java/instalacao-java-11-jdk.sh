#!/bin/bash

### Verificação se é sudo

if [ "$EUID" -eq 0 ]; then
    echo "Este Script deve ser executado com seu usuário do sistema, não o execute com sudo."
    exit
fi

#### Geral

echo -e "\n\n Criando diretórios \n\n"

mkdir ~/Desenv
mkdir ~/Desenv/java


echo -e "\n\n Diretórios criados em ~/Desenv \n\n"

ls -la ~/Desenv/

# JDK 11

# Baixando pacote

echo -e "\n\n Iniciando instalação do Java 11 ( JDK AdoptOpenJDK Hotspot ) \n\n"

wget --progress=bar https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.6%2B10/OpenJDK11U-jdk_x64_linux_hotspot_11.0.6_10.tar.gz -O ~/Desenv/java/jdk-11.0.6.tar.gz

tar -zxvf ~/Desenv/java/jdk-11.0.6.tar.gz -C ~/Desenv/java/

rm ~/Desenv/java/jdk-11.0.6.tar.gz


echo -e "\n #### Variáveis setadas pelo script de instalação do java 11 \n" >> ~/.profile

echo 'JAVA_HOME=~/Desenv/java/jdk-11.0.6+10' >> ~/.profile
echo 'PATH=$PATH:$JAVA_HOME/bin' >> ~/.profile

echo -e "\n ########################################################### \n" >> ~/.profile

source ~/.profile

java -version

echo -e "Instalação do Java 11 completa."

