# Pré-requisitos

sudo apt-get install build-essential libz-dev zlib1g-dev

# Baixando pacote

echo -e "\n\n Iniciando instalação da GraalVM 19.3.1 - JDK 11 \n\n"

wget --progress=bar https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-19.3.1/graalvm-ce-java11-linux-amd64-19.3.1.tar.gz -O ~/Desenv/java/graalvm-ce-java11-linux-amd64-19.3.1.tar.gz

tar -zxvf ~/Desenv/java/graalvm-ce-java11-linux-amd64-19.3.1.tar.gz -C ~/Desenv/java/

rm ~/Desenv/java/graalvm-ce-java11-linux-amd64-19.3.1.tar.gz


echo -e "\n #### Variáveis setadas pelo script de instalação da GraalVM \n" >> ~/.profile

echo 'GRAALVM_HOME=~/Desenv/java/graalvm-ce-java11-19.3.1' >> ~/.profile

echo -e "\n ########################################################### \n" >> ~/.profile

source ~/.profile

echo -e "\n\n Instalando Feature native-image da GraalVM \n\n"

$GRAALVM_HOME/bin/gu install native-image

$GRAALVM_HOME/bin/native-image --version

echo -e "\n Instalação da GraalVM completa."