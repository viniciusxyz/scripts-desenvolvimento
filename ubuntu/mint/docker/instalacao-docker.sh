sudo curl -fsSL https://get.docker.com | sh

sudo usermod -aG docker $USER

sudo docker run hello-world

sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

docker --version
docker-compose --version